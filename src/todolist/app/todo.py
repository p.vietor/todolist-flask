from flask import Flask
from flask_restx import Resource, fields, marshal, Api

from domain.todo import Todo
from app.app import get_app, get_api, get_store

# Flask app
app = get_app()

# Restx config (for documentation/api binding)
api = get_api()

# Storage management
todo_store = get_store()

# API /exchange domain representation (DTO)
todo_fields = api.model('Todo', {
    'id': fields.String,
    'content': fields.String,
    'completed': fields.Boolean
})

error_fields = api.model('Error', {
    'message': fields.String(default='Bad Request')
})

# HTTP controllers section
@api.route('/api/todo', endpoint='todos')
class TodoController(Resource):

    @api.response(200, 'Success', todo_fields, as_list=True)
    def get(self):
        payload = todo_store.get_todo()
        print(payload)
        return marshal(payload, todo_fields)
    
    @api.expect(todo_fields, validate=False)
    @api.response(201, 'Created', todo_fields)
    @api.response(400, 'Bad Request', error_fields)
    def post(self):
        payload = api.payload
        try:
          todo = Todo(payload['id'], payload['content'], payload['completed'])
        except:
            return { "message": "Bad request - Payload could not be deserialized" }, 400
        stored_todo = todo_store.add_todo(todo)
        return marshal(stored_todo, todo_fields), 201


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5000, debug=False)
